package fibonacci;

import java.math.BigInteger;

public class FibonacciNumberPrinter {

    /**
     * Prints a specific amount of fibonacci numbers from 0.
     * 
     * @param digitInt The amount of fibonacci numbers to be printed.
     */
    public void printFirstFibonacciNumbers(int digitInt) {
        BigInteger digitBigInt = BigInteger.valueOf(digitInt);
        BigInteger nM2 = BigInteger.ZERO;
        BigInteger nM1 = BigInteger.ONE;
        BigInteger n = BigInteger.ZERO;
        
        if (digitBigInt.compareTo(BigInteger.ZERO) <= 0) {
            System.out.print(BigInteger.valueOf(-1));
        }

        if (digitBigInt.compareTo(BigInteger.ZERO) > 0) {
            System.out.print(nM2);
        }
        if (digitBigInt.compareTo(BigInteger.ONE) > 0) {
            System.out.print(", " + nM1);
        }
        if (digitBigInt.compareTo(BigInteger.valueOf(2)) > 0) {
            for (int i = 0; i < digitBigInt.intValue() - 2; i++) { // - 2 because we already printed the first two
                n = nM2.add(nM1);
                System.out.print(", " + n);
                nM2 = nM1;
                nM1 = n;
            }
        }
    }

    public BigInteger computeFibonacciNumbers(int digitInt) {
        BigInteger digitBigInt = BigInteger.valueOf(digitInt);
        BigInteger nM2 = BigInteger.ZERO;
        BigInteger nM1 = BigInteger.ONE;
        BigInteger n = BigInteger.ZERO;

        if (digitBigInt.compareTo(BigInteger.ONE) == 0) {
            return nM2;
        }
        if (digitBigInt.compareTo(BigInteger.valueOf(2)) == 0) {
            return nM1;
        }
        if (digitBigInt.compareTo(BigInteger.valueOf(2)) > 0) {
            for (int i = 0; i < digitBigInt.intValue() - 2; i++) { // - 2 because we already printed the first two
                n = nM2.add(nM1);
                nM2 = nM1;
                nM1 = n;
            }
            return n;
        }
        return BigInteger.valueOf(-1);
    }
}
