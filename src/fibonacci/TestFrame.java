package fibonacci;

public class TestFrame {

    public static void main(String[] args) {
        FibonacciNumberPrinter fnp = new FibonacciNumberPrinter();
        fnp.printFirstFibonacciNumbers(-1);
        System.out.println();
        System.out.println(fnp.computeFibonacciNumbers(-1));
        System.out.println("-------------------------------------");
        fnp.printFirstFibonacciNumbers(0);
        System.out.println();
        System.out.println(fnp.computeFibonacciNumbers(0));
        System.out.println("-------------------------------------");
        fnp.printFirstFibonacciNumbers(1);
        System.out.println();
        System.out.println(fnp.computeFibonacciNumbers(1));
        System.out.println("-------------------------------------");
        fnp.printFirstFibonacciNumbers(2);
        System.out.println();
        System.out.println(fnp.computeFibonacciNumbers(2));
        System.out.println("-------------------------------------");
        fnp.printFirstFibonacciNumbers(10);
        System.out.println();
        System.out.println(fnp.computeFibonacciNumbers(10));
        System.out.println("-------------------------------------");
        fnp.printFirstFibonacciNumbers(100);
        System.out.println();
        System.out.println(fnp.computeFibonacciNumbers(100));
    }

}
